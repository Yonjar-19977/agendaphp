const formularioContactos = document.querySelector('#contacto'),
    listadoContactos = document.querySelector('#listado-contactos tbody');



eventListeners();

function eventListeners() {
     // Cuando el formulario de crear o editar se ejecuta
        formularioContactos.addEventListener('submit', leerFormulario);

        //listener para eliminar un boton
        if (listadoContactos) {
            listadoContactos.addEventListener('click', eliminarContacto);
        }
        

    }
    function leerFormulario(e) {
        e.preventDefault();

        // Leer los datos de los inputs
        const nombre = document.querySelector('#nombre').value,
            empresa = document.querySelector('#empresa').value,
            telefono = document.querySelector('#telefono').value,
            accion = document.querySelector('#accion').value;
    
    if(nombre === '' || empresa === '' || telefono === '') {
        // 2 parametros : texto y clase
        mostrarNotificacion('Todos los Campos son Obligatorios', 'error');
    }else {
        //pasan la validacion
        const infoContacto = new FormData();
        infoContacto.append('nombre', nombre);
        infoContacto.append('empresa', empresa);
        infoContacto.append('telefono', telefono);
        infoContacto.append('accion', accion);

        console.log(...infoContacto);

        if(accion == 'crear'){
            //creamos un nuevo contacto
            insertarBD(infoContacto);
        } else {
            // editar el contacto
            //leer id
            const idRegistro = document.querySelector('#id').value;
            infoContacto.append('id', idRegistro);
            actualizarRegistro(infoContacto);
        }
    }
    
}
/** insertar en la base de datos via ajax */
function insertarBD(datos){
    // llamados ajax

    //crear objeto
    const xhr = new XMLHttpRequest();
    //abrir la conexion
    xhr.open('POST', 'inc/modelos/modelo-contactos.php', true)
    //pasar los datos
    xhr.onload = function(){
        if(this.status === 200){
            console.log(JSON.parse(xhr.responseText));
            // leemos las respuesta de php
            const respuesta = JSON.parse(xhr.responseText);

            //insertar un nuevo elemento a la tabla
            const nuevoContacto = document.createElement('tr');

            nuevoContacto.innerHTML = `
                <td>${respuesta.datos.nombre}</td>
                <td>${respuesta.datos.empresa}</td>
                <td>${respuesta.datos.telefono}</td>
            `;
            // crear contenedor de los botones
            const contenedorAcciones = document.createElement('td');

            //crear el icono de editar
            const iconoEditar = document.createElement('i');
            iconoEditar.classList.add('fas','fa-pen-square');

            //crear el enlace para editar
            const btnEditar = document.createElement('a');
            btnEditar.appendChild(iconoEditar);
            btnEditar.href = `editar.php?id=${respuesta.datos.id_insertado}`;
            btnEditar.classList.add('btn', 'btn-editar');

            //agreagarlo al padre
            contenedorAcciones.appendChild(btnEditar);

            // crear el icono de eliminar

            const iconoEliminar = document.createElement('i');
            iconoEliminar.classList.add('fas','fa-trash-alt');

            //crear el boton eliminar
            const btnEliminar = document.createElement('button');
            btnEliminar.appendChild(iconoEliminar);
            btnEliminar.setAttribute('data_id', respuesta.datos.id_insertado);
            btnEliminar.classList.add('btn', 'btn-borrar');

            //agregar al padre
            contenedorAcciones.appendChild(btnEliminar);

            //agregarlo al tr
            nuevoContacto.appendChild(contenedorAcciones);

            //agragarlo a los contactos
            listadoContactos.appendChild(nuevoContacto);

            //resatear el formulario
            document.querySelector('form').reset();
            //motrar la notificacion
            mostrarNotificacion('Contacto Creado Correctamente', 'correcto')
        }
    }
    //enviar los datos
    xhr.send(datos); 
}

    function actualizarRegistro(datos) {
        // crear el objeto
        const xhr = new XMLHttpRequest();

        // abrir la conexión
        xhr.open('POST', 'inc/modelos/modelo-contactos.php', true);

        // leer la respuesta
        xhr.onload = function() {
            if(this.status === 200) {
                const respuesta = JSON.parse(xhr.responseText);

                if(respuesta.respuesta === 'correcto'){
                       // mostrar notificación de Correcto
                    mostrarNotificacion('Contacto Editado Correctamente', 'correcto');
                } else {
                       // hubo un error
                    mostrarNotificacion('Hubo un error...', 'error');
                    }
                  // Después de 3 segundos redireccionar
                    setTimeout(() => {
                    window.location.href = 'index.php';
                }, 4000);
            }
        }

        // enviar la petición
        xhr.send(datos);
    }

// eliminar el contacto
function eliminarContacto(e) {
    if(e.target.parentElement.classList.contains('btn-borrar')) {
        // tomar el ID
        const id = e.target.parentElement.getAttribute('data-id');
        //console.log(id);

        //preguntar al usuario
        const respuesta = confirm('¿Estas Seguro(a) ?');

        if (respuesta) {
            // llamado a ajax
            // crear el objeto
            const xhr = new XMLHttpRequest();

            //abrir la conexion
            xhr.open('GET', `inc/modelos/modelo-contactos.php?id=${id}&accion=borrar`, true);

            //leer respuesta
            xhr.onload = function() {
                if (this.status === 200) {
                    const resultado = JSON.parse(xhr.responseText);
                    
                    if (resultado.respuesta == 'correcto') {
                        //eliminar el registro del doom
                        console.log(e.target.parentElement.parentElement.parentElement);
                        e.target.parentElement.parentElement.parentElement.remove();
                        //mostrar notificacion
                        mostrarNotificacion('Contacto eliminado', 'correcto');

                    } else {
                        // Mostramos una notificacion
                        mostrarNotificacion('Hubo un error..', 'error');
                    }
                }
            }
            // enviar la peticion
            xhr.send();
        }  
        
    }
}

// notificacion en pantalla
function mostrarNotificacion(mensaje, clase) {
    const notificacion = document.createElement('div');
    notificacion.classList.add(clase,'notificacion', 'sombra');
    notificacion.textContent = mensaje;

      // formulario
    formularioContactos.insertBefore(notificacion, document.querySelector('form legend'));
    
    //ocultar y mostrar la notificion
    setTimeout(() => {
        notificacion.classList.add('visible');

        setTimeout(() => {
            notificacion.classList.remove('visible');
            setTimeout(() => {
                notificacion.remove();
            }, 500);
        }, 3000);
    }, 100);

}

